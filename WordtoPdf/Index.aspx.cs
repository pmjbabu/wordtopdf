﻿using DocumentFormat.OpenXml.Packaging;
using System;
using System.Xml;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WordtoPdf
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string doc = "E:\\Others\\GitDev\\WordtoPDF\\WordtoPdf\\Files\\160.docx";
            string pdf = "E:\\Others\\GitDev\\WordtoPDF\\WordtoPdf\\Files\\";
            string logo = "E:\\Others\\GitDev\\WordtoPDF\\WordtoPdf\\Files\\sspl.png";
            string clientimgPath = "E:\\Others\\GitDev\\WordtoPDF\\WordtoPdf\\Files\\Technip.png";
            
            string extension = System.IO.Path.GetExtension(doc);
            if (extension == ".doc" || extension == ".docx")
            {
                try
                {
                    SearchAndReplace(doc);
                    SaveImage(doc, "PID", logo, clientimgPath);

                }
                catch (Exception ex)
                {
                    string str = ex.Message;
                }
            }
        }
        public static void SearchAndReplace(string document)
        {
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
            {
                string docText = null;
                foreach (var headerPart in wordDoc.MainDocumentPart.HeaderParts)
                {
                    //Gets the text in headers
                    foreach (var currentText in headerPart.RootElement.Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>())
                    {
                        currentText.Text = currentText.Text.Replace("sysno", "AAAA");
                        currentText.Text = currentText.Text.Replace("tagno", "BBBBBBB");
                        currentText.Text = currentText.Text.Replace("subsysdesc", "BBBBBBB");
                    }
                }
              
            }
        }
        private void SaveImage(string document, string tagName, string imgPath,string clientimgPath)
        {
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
            {
                foreach (HeaderPart headerPart in wordDoc.MainDocumentPart.HeaderParts)
                {
                    List<ImagePart> imgParts = new List<ImagePart>(headerPart.ImageParts);
                    int count = 0;
                    foreach (ImagePart img in imgParts)
                    {

                        if (img.ContentType == "image/png") // sspl logo
                        {
                            using (var writer = new BinaryWriter(img.GetStream()))
                            {
                                writer.Write(File.ReadAllBytes(imgPath));
                            }
                        }
                        else
                        {
                            using (var writer = new BinaryWriter(img.GetStream()))
                            {
                                writer.Write(File.ReadAllBytes(clientimgPath));
                            }
                        }
                        
                    }
                }
            }
        }


    }
}